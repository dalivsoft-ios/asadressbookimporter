//
//  ASAdressBookImporter.h
//  ContactImporter
//
//  Created by Maxim Deygin on 11.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>

@interface ASAdressBookImporter : NSObject
{
    NSMutableArray *_contacts;
}
@property(nonatomic,strong) NSMutableArray *contacts;

-(void)importAddresBook;
-(NSArray*)getContacts;
@end
