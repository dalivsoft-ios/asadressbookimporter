//
//  ASAdressBookImporter.m
//  ContactImporter
//
//  Created by Maxim Deygin on 11.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ASAdressBookImporter.h"

@implementation ASAdressBookImporter
@synthesize contacts = _contacts;

-(id)init
{
    self.contacts = [[NSMutableArray alloc] init];
    [self importAddresBook];
    return self;
}

-(void)importAddresBook
{
    ABAddressBookRef addressBook = ABAddressBookCreate();
    CFArrayRef arrTemp=ABAddressBookCopyArrayOfAllPeople(addressBook);
    
    
    CFIndex contactCount = CFArrayGetCount(arrTemp);
    
    for(int i = 0; i<contactCount; i++)
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        ABRecordRef ref = CFArrayGetValueAtIndex(arrTemp, i);
        
        if(ABRecordGetRecordType(ref)==0)
        {
            
            ABMultiValueRef mult;
            NSArray *arr;
            CFStringRef str = ABRecordCopyValue(ref, kABPersonFirstNameProperty);
            NSString *nsstr= [NSString stringWithFormat:@"%@",str];
            [dict setValue:nsstr forKey:@"firstName"];
            
            str = ABRecordCopyValue(ref, kABPersonLastNameProperty);
            nsstr =  [ NSString stringWithFormat:@"%@",str];
            [dict setValue:nsstr forKey:@"lastName"];
            
            str = ABRecordCopyValue(ref, kABPersonMiddleNameProperty);
            nsstr =  [ NSString stringWithFormat:@"%@",str];
            [dict setValue:nsstr forKey:@"middleName"];
            
            str = ABRecordCopyValue(ref, kABPersonOrganizationProperty);
            nsstr =  [ NSString stringWithFormat:@"%@",str];
            [dict setValue:nsstr forKey:@"Compapny"];
            
            
            mult = ABRecordCopyValue(ref, kABPersonEmailProperty);
            [dict setValue:arr forKey:@"email"];
            NSMutableArray *eMailTypes=[[NSMutableArray alloc] init];
            int num =  ABMultiValueGetCount(mult);
            for (int i =0; i<num; i++)
            {
                str = ABMultiValueCopyLabelAtIndex(mult, i);
                nsstr = [NSString stringWithFormat:@"%@",str];
                [eMailTypes addObject:nsstr];
            }
            NSArray *eMails = (__bridge NSArray*)ABMultiValueCopyArrayOfAllValues(mult);
            NSMutableArray *emailsWithTypes = [[NSMutableArray alloc] init];
            for(int i = 0; i<[eMailTypes count];i++)
            {
                NSDictionary *emailWithtype = [NSDictionary dictionaryWithObjectsAndKeys:[eMailTypes objectAtIndex:i],@"type",[eMails objectAtIndex:i],@"addres",nil];
                [emailsWithTypes addObject:emailWithtype];
            }
            [dict setValue:emailsWithTypes forKey:@"email"];
            
            
            
            mult = ABRecordCopyValue(ref, kABPersonPhoneProperty);
            num =  ABMultiValueGetCount(mult);
           
            NSMutableArray *PhoneTypes = [[NSMutableArray alloc] init];
            for (int i =0; i<num; i++)
            {
                str = ABMultiValueCopyLabelAtIndex(mult, i);
                nsstr = [NSString stringWithFormat:@"%@",str];
                [PhoneTypes addObject:nsstr];
            }
            
            NSArray *phones = (__bridge NSArray*)ABMultiValueCopyArrayOfAllValues(mult);
            NSMutableArray *phonesWithTypes = [[NSMutableArray alloc] init];
            for(int i = 0; i<[PhoneTypes count];i++)
            {
                NSDictionary *phoneWithType = [NSDictionary dictionaryWithObjectsAndKeys:[PhoneTypes objectAtIndex:i],@"type",[phones objectAtIndex:i],@"number",nil];
                [phonesWithTypes addObject:phoneWithType];
            }
            
            
            [dict setValue:phonesWithTypes forKey:@"phone"];
            
            mult = ABRecordCopyValue(ref, kABPersonURLProperty);
            num =  ABMultiValueGetCount(mult);
            NSMutableArray *URLTypes = [[NSMutableArray alloc] init];
            for (int i =0; i<num; i++)
            {
                str = ABMultiValueCopyLabelAtIndex(mult, i);
                nsstr = [NSString stringWithFormat:@"%@",str];
                [URLTypes addObject:nsstr];
            }
            
            NSArray *URLs = (__bridge NSArray*)ABMultiValueCopyArrayOfAllValues(mult);
            NSMutableArray *URLsWithTypes = [[NSMutableArray alloc] init];
            for(int i = 0; i<[URLTypes count];i++)
            {
                NSDictionary *URLWithType = [NSDictionary dictionaryWithObjectsAndKeys:[URLTypes objectAtIndex:i],@"type",[URLs objectAtIndex:i],@"URL",nil];
                [URLsWithTypes addObject:URLWithType];
            }
            [dict setValue:URLsWithTypes forKey:@"URL"];
            
            
            mult = ABRecordCopyValue(ref, kABPersonInstantMessageProperty);
            num =  ABMultiValueGetCount(mult);
            NSMutableArray *IMTypes = [[NSMutableArray alloc] init];
            for (int i =0; i<num; i++)
            {
                str = ABMultiValueCopyLabelAtIndex(mult, i);
                nsstr = [NSString stringWithFormat:@"%@",str];
                [IMTypes addObject:nsstr];
            }
            
            NSArray *IMs = (__bridge NSArray*)ABMultiValueCopyArrayOfAllValues(mult);
            NSMutableArray *IMsWithTypes = [[NSMutableArray alloc] init];
            for(int i = 0; i<[IMTypes count];i++)
            {
                NSDictionary *IMWithType = [NSDictionary dictionaryWithObjectsAndKeys:[IMTypes objectAtIndex:i],@"type",[IMs objectAtIndex:i],@"Messenger",nil];
                [IMsWithTypes addObject:IMWithType];
            }
            
            [dict setValue:IMsWithTypes forKey:@"IM"];
            
            [self.contacts addObject:dict];   
        }
        else 
        {
            //not a person!
        }
    }
}
-(NSArray *)getContacts
{
    if([self.contacts count])
    {
        return self.contacts;
    }
    else 
    {
        return nil;
    }
}

@end
