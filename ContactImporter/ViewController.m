//
//  ViewController.m
//  ContactImporter
//
//  Created by Maxim Deygin on 04.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
//#import <AddressBook/AddressBook.h>
#import "ASAdressBookImporter.h"
@interface ViewController ()

@end

@implementation ViewController



- (void)viewDidLoad
{
      
    [super viewDidLoad];

    ASAdressBookImporter *bookImporter = [[ASAdressBookImporter alloc] init];
    NSArray *arr = [[NSArray alloc] initWithArray:[bookImporter getContacts]]; 
    NSLog(@"%@",arr);
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
